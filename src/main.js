import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";


import "@/styles/index.scss";
import '@splidejs/splide/dist/css/themes/splide-default.min.css';

import '@/filters';

// componentes globals
import '@/components/globals';

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
}).$mount("#app");
