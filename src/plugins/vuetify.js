import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

// Vue Notifications
import Notifications from "vue-notification";

Vue.use(Vuetify);
Vue.use(Notifications);

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: "#000",
				secondary: "#BE1E2D",
				accent: "#636466",
			},
		},
	},
});
