export default [
    {
        text: "Catálogo",
        disabled: false,
        exact: true,
        to: {
            name: "ListProductsRoute",
        },
    },
    {
        text: "Carrito de compra",
        disabled: false,
        exact: true,
        to: {
            name: "ShoppingCartRoute",
        },
    },
    {
        text: "Checkout",
        disabled: true,
    },
]; 