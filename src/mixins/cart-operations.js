import OrderService from "@/services/Order.service";
import { notifySuccess, notifyError } from "@/helpers/notify";

const CartOperations = {
    data: () => ({
        co_loading: false
    }),
    methods: {
        async $_removeItem(sku, orderId) {
            this.co_loading = true;
            try {
                const { data: ResponseRemove } = await OrderService.RemoveProduct(
                    sku,
                    orderId
                );
                const { items, total, subtotal, total_igv } = ResponseRemove;
                this.$store.commit("UPDATE_CART", {
                    products: items,
                    total,
                    subtotal,
                    total_igv,
                });
                notifySuccess("Carrito Actualizado!");
            } catch (error) {
                notifyError("Ooops! ha ocurrido un error, intentelo nuevamente!");
            } finally {
                this.co_loading = false;
            }
        },
    },
};

export default CartOperations;