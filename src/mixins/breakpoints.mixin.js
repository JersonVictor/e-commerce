const BreakPointsMixin = {
	computed: {
		$_smAndDown() {
			return this.$vuetify.breakpoint.smAndDown;
		},
		$_mdAndUp() {
			return this.$vuetify.breakpoint.mdAndUp;
		},
	},
};

export default BreakPointsMixin;
