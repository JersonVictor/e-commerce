/**
 * Convierte el valor a un string con formato ',00'

 */

export default function (value) {
    if (!value) return "";

    const num = typeof (value) === 'string' ? parseFloat(value) : value;

    return num.toLocaleString("us-US", {
        style: "currency",
        currency: "PEN",
    });

    // return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}
