import Store from "@/store";
import axios from "axios";

const http = axios.create({
    baseURL: process.env.VUE_APP_API,
    headers: {
        "Content-type": "application/json",
    },
});

http.interceptors.request.use(function (config) {
    const token = Store.state.session.token;

    if (token) config.headers.Authorization = `Token ${token}`;

    return config;
});

export default http;
