import http from "./http";

export const setTokenToHttp = () => {
	const token = window.localStorage.getItem("token");
	if (token) {
		http.defaults.headers.common["Authorization"] = `Token ${token}`;
	}
};
