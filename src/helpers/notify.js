import Vue from 'vue';

export const notifySuccess = (message) => {
	Vue.notify({
		group: "notify",
		text: message,
		type: "success",
	});
};

export const notifyError = (message) => {
	Vue.notify({
		group: "notify",
		text: message,
		type: "error",
	});
};
