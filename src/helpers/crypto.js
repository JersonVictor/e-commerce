import jwt from "jsonwebtoken";


export const encrypt = (data) => {
    return jwt.sign(data, process.env.VUE_APP_KEY_NAME_JWT);
}


export const verify = (token) => jwt.verify(token, process.env.VUE_APP_KEY_NAME_JWT);