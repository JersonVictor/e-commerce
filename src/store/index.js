import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
});

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        session: {
            user: null,
            token: null,
        },
        cart: {
            products: [],
            total: 0,
            total_igv: 0,
            subtotal: 0,
        },
        orderId: null,
        purchase_number: null,
    },
    mutations: {
        UPDATE_SESSION(state, session) {
            state.session = { ...state.session, ...session };
        },
        CLOSE_SESSION(state) {
            state.session = { user: null, token: null };

            // CLEAR LOCALSTORAGE
            window.localStorage.removeItem("vuex");
            window.localStorage.clear();
        },
        UPDATE_USER(state, user) {
            state.user = user;
        },
        UPDATE_CART(state, payload) {
            state.cart = { ...state.cart, ...payload };
        },
        CLEAN_CART(state) {
            state.cart = {
                products: [],
                total: 0,
                total_igv: 0,
                subtotal: 0,
            };
        },
        SET_ORDER_ID(state, payload) {
            state.orderId = payload;
        },
        SET_PURCHASE_NUMBER(state, payload) {
            state.purchase_number = payload;
        },
        UPDATE_PRODUCT(state, payload) {
            const { item_sku } = payload;
            const index_product = state.cart.products.indexOf(
                (product) => product.item_sku === item_sku
            );
            state.cart.products[index_product] = payload;
        },
    },
    getters: {
        /** Session Activa */
        isAuthenticated(state) {
            return state.session.user && state.session.token;
        },
        cartCount(state) {
            return state.cart.products.length.toString();
        },
        hasOrder(state) {
            return !!state.orderId;
        },
        cartProducts(state) {
            return state.cart.products.map((item, index) => ({ ...item, index: index + 1 }));
        },
        total(state) {
            return state.cart.total.toFixed(2);
        },
        subtotal(state) {
            return state.cart.subtotal.toFixed(2);
        },
        totalIgv(state) {
            return state.cart.total_igv.toFixed(2);
        },
    },
    actions: {},
    modules: {},
    plugins: [vuexLocal.plugin],
});
