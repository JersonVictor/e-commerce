export const addZeroes = (num) => {
	//set everything to at least two decimals; removs 3+ zero decimasl, keep non-zero decimals
	var new_value = num * 1; //removes trailing zeros
	new_value = new_value + ""; //casts it to string

	const pos = new_value.indexOf(".");
	if (pos == -1) new_value = new_value + ".00";
	else {
		var integer = new_value.substring(0, pos);
		var decimals = new_value.substring(pos + 1);
		while (decimals.length < 2) decimals = decimals + "0";
		new_value = integer + "." + decimals;
	}
	return new_value;
};

export const removePropsBlank = (obj = {}) => {
	for (var propName in obj) {
		if (obj[propName] === null || obj[propName] === undefined) {
			delete obj[propName];
		}
	}
	return obj;
};

export const toQueryParams = (obj = {}) => {
	const keys = Object.keys(obj);

	if (keys.length === 0) return "";

	return `?${keys.map((key) => key + "=" + obj[key]).join("&")}`;
};
