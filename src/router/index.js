import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "HomeRoute",
        component: () => import("@/views/Home/index"),
    },
    {
        path: "/productos",
        name: "ListProductsRoute",
        component: () => import("@/views/Products/ListProducts.vue"),
    },
    {
        path: "/producto/:productId",
        name: "DetailProductRoute",
        component: () => import("@/views/Products/DetailProduct.vue"),
    },
    {
        path: "/blog",
        name: "BlogRoute",
        component: () => import("@/views/Blog/index"),
    },
    {
        path: "/about-us",
        name: "AboutUsRoute",
        component: () => import("@/views/AboutUs/index"),
    },
    {
        path: "/contact",
        name: "ContactRoute",
        component: () => import("@/views/Contact/index"),
    },
    {
        path: "/login",
        name: "LoginRoute",
        component: () => import("@/views/Login/index"),
    },
    {
        path: "/registro",
        name: "RegisterRoute",
        component: () => import("@/views/Register/index"),
    },
    {
        path: "/carrito-compra",
        name: "ShoppingCartRoute",
        component: () => import("@/views/ShoppingCart/index"),
    },
    {
        path: "/entrega",
        name: "DeliveryRoute",
        component: () => import("@/views/Delivery/index"),
    },
    {
        path: "/checkout",
        name: "CheckoutRoute",
        component: () => import("@/views/Checkout/index"),
    },
    {
        path: "/ordenes",
        name: "OrderRoute",
        component: () => import("@/views/Order/index"),
    },
    {
        path: "/ordenes/:id",
        name: "OrderDetailRoute",
        component: () => import("@/views/Order/OrderDetail"),
    },
    {
        path: "/orden-completado",
        name: "OrderFinishRoute",
        component: () => import("@/views/OrderFinish"),
    },
    {
        path: "/terminos-condiciones",
        name: "TermsConditionsRoute",
        component: () => import("@/views/TermsAndConditions/index"),
    },

];

// TODO: AÑADIR PAGE 404 NOT FOUND

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router;
