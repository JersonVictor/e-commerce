const data = [
  {
    id: 1,
    titulo: "EMPRESA A",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 2,
    titulo: "EMPRESA B",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 3,
    titulo: "EMPRESA C",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 4,
    titulo: "EMPRESA F",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 5,
    titulo: "EMPRESA G",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 6,
    titulo: "EMPRESA H",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
  {
    id: 7,
    titulo: "EMPRESA I",
    fundadores: "Fundador A, Fundador B",
    logo: "https://cdn.vuetifyjs.com/images/cards/cooking.png",
    resumen:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem quaconsectetur delectus quia aliquam dolorem ipsam, omnis fugit",
  },
];

const CompanyService = {
  ListCompanies() {
    return Promise.resolve(data);
  },
};

export default CompanyService;
