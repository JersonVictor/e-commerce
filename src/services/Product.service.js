import http from "@/helpers/http";

const ProductService = {
	ListProducts(suffixQueryParams = "") {
		return http.get(`/products/api/products${suffixQueryParams}`);
	},

	GetProduct(skuProduct) {
		return http.get(`/products/api/products/${skuProduct}`);
	},

	ListCategories() {
		return http.get(`/products/api/categories`);
	},

	ListBrands() {
		return http.get(`/products/api/brands`);
	},
};

export default ProductService;
