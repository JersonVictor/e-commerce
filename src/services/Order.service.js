import http from "@/helpers/http";

const OrderService = {
	CreateOrder() {
		return http.post("asbasdasd");
	},
	AddProduct(sku, orderId) {
		return http.post(`/orders/api/add_to_cart/${sku}`, { order: orderId });
	},
	DeleteOneProduct(sku, orderId) {
		return http.post(`/orders/api/remove_item_from_cart/${orderId}/${sku}`);
	},
	RemoveProduct(sku, orderId) {
		return http.post(`/orders/api/remove_from_cart/${orderId}/${sku}`);
	},

	PaymentOrder(orderId) {
		return http.post(`/orders/api/payment/${orderId}`);
	},

	ListMeOrders() {
		return http.get(`/orders/api/`);
	},
	OrderDetail(uid) {
		return http.get(`/orders/api/${uid}`);
	},
};

export default OrderService;
