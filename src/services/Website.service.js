import http from "@/helpers/http";

const WebsiteService = {
	/**
	 *  Enviar Mensaje Contact
	 * @param {*} payload
	 * @returns
	 */
	SendMessage(payload) {
		return http.post("/website/api/contact/", payload);
	},
	ListCompanies() {
		return http.get("/website/api/blog/");
	},
};

export default WebsiteService;
