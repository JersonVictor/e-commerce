import http from "@/helpers/http";

const CheckoutService = {
    getTokenSecurity() {
        return http.get("/orders/api/payment/accesstoken");
    },
    getTokenSession(payload) {
        return http.post("/orders/api/payment/sessiontoken", payload);
    },
    getAutorization(payload) {
        return http.post("/orders/api/payment/authorization", payload);
    }
};

export default CheckoutService;

