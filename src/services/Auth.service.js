import http from "@/helpers/http";

const AuthService = {
  RegisterUser(payload) {
    return http.post("/users/api/create/", payload);
  },
  LoginUser(payload) {
    return http.post("/users/api/token/", payload);
  },
};

export default AuthService;
